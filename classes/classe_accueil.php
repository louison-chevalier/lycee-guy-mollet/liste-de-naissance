<?php class accueil
{
    private $selectAll;
    private $updateAll;
    private $selectOne; 
    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db)
    {
    $this->selectAll = $db->prepare("SELECT * FROM accueil");


    $this->updateAll = $db->prepare("update accueil set pere=:pere, mere=:mere, enfant=:enfant,  mail=:mail, date=:date, lieu=:lieu, titreprincipal=:titreprincipal, soustitre=:soustitre, titre1=:titre1, titre2=:titre2, photo1=:photo1, photo2=:photo2, photo3=:photo3, photo4=:photo4 where nom=:nom ") ; 

     $this->selectOne = $db->prepare("select * from type where nom=:nom");
    }
     
    

    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
        
    }
    
    public function updateAll($nom, $pere, $mere, $enfant, $mail, $date, $lieu, $titreprincipal, $soustitre, $titre1, $titre2, $photo1, $photo2, $photo3, $photo4){
    $this->updateAll->execute(array(':nom'=>$nom,':pere'=>$pere, ':mere'=>$mere, ':enfant'=>$enfant,':mail'=>$mail, ':date'=>$date, ':lieu'=>$lieu, ':titreprincipal'=>$titreprincipal, ':soustitre'=>$soustitre, ':titre1'=>$titre1, ':titre2'=>$titre2, ':photo1'=>$photo1, ':photo2'=>$photo2, ':photo3'=>$photo3, ':photo4'=>$photo4));
    return $this->updateAll->rowCount();
}


public function selectOne($nom){ 
    $this->selectOne->execute(array(':nom'=>$nom)); 
    return $this->selectOne->fetch();
}
    
} ?>