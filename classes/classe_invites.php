<?php class Invites
{
    private $insertAll;
    private $selectAll;
    private $deleteOne;
    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db)
    {
    $this->insertAll = $db->prepare("INSERT INTO invites (email, nom, prenom, mdp) VALUES (:email,:nom, :prenom, :mdp)");
    $this->deleteOne = $db->prepare("delete from invites where email=:email ") ; 
    $this->selectAll = $db->prepare("SELECT * FROM invites Order by nom ASC");
    }
     
    
    public function insertAll($email, $nom , $prenom, $mdp)
    {
    $this->insertAll->execute(array(''
        . ':email'=>$email,
          ':nom'=>$nom,
          ':prenom' => $prenom,
          ':mdp' => $mdp));
    return $this->insertAll->rowCount();}
        
    
    public function deleteOne($email){
    $this->deleteOne->execute(array(':email'=>$email));
    return $this->deleteOne->rowCount();
    }


    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
        
    }
    
    
} ?>