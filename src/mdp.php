<?php

// Fonction qui creer et retourne un mot de passe de taille souhaiter
function genererMdp($nbCar){

    //Initialisation  
    $lettre='a';
    $mdp='';

    // le tableau $tlettre reçoit toutes les lettres de l alphabet
    for ($i=0;$i<26;$i++){
        $tlettre[$i]=$lettre;
        $lettre++;
    }
    //(test) var_dump($tlettre);

    //la variable mdp recoit un caractere pris aleatoirement dans le tableau tlettre autant de fois que le nombre $nbCar
    for($i=0;$i<$nbCar;$i++){
        $mdp.=$tlettre[rand(0,25)];	
    }

    return $mdp;		
}

?>
