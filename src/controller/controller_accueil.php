<?php

 	
 function Accueil($db){

 	$accueil = new accueil($db);
    $listeaccueil = $accueil->selectAll();
    foreach($listeaccueil as $unAccueil){

 echo '

	<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(assets/images/'.utf8_encode($unAccueil['photo1'] ).');" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>'.utf8_encode($unAccueil['titreprincipal'] ).' <span style="color:#FF0F8F;">'.utf8_encode($unAccueil['enfant'] ).'</span></h1>
							<h2>'.utf8_encode($unAccueil['soustitre'] ).'</h2>
							<div class="simply-countdown simply-countdown-one"></div>
							<p><a href="index.php?page=cadeaux" class="btn btn-default btn-sm">Liste des cadeaux</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-couple">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h3>'.utf8_encode($unAccueil['titre1'] ).'</h3>
					<p>'.utf8_encode($unAccueil['titre2'] ).'</p>
				</div>
			</div>
			<div class="couple-wrap animate-box">
				<div class="couple-half">
					<div class="groom">
						<img src="assets/images/groom.jpg" alt="groom" class="img-responsive">
					</div>
					<div class="desc-groom">
						<br>
						<br>
						<h3>'.utf8_encode($unAccueil['nom'] ).' '.utf8_encode($unAccueil['pere'] ).'</h3>
					</div>
				</div>
				<p class="heart text-center"><i class="icon-heart2"></i></p>
				<div class="couple-half">
					<div class="bride">
						<img src="assets/images/bride.jpg" alt="groom" class="img-responsive">
					</div>
					<div class="desc-bride">
					<br>
					<br>
						<h3>'.utf8_encode($unAccueil['nom'] ).' '.utf8_encode($unAccueil['mere'] ).'</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	

';
after();
}
}

function after(){
	echo'

	<footer id="fh5co-footer" role="contentinfo">
		<div class="container">

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2017 - Challenge 2 - Borniche Léo - Chevalier Louison - Gorlier Valentin</small> 
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="assets/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="assets/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="assets/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="assets/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="assets/js/jquery.countTo.js"></script>

	<!-- Stellar -->
	<script src="assets/js/jquery.stellar.min.js"></script>
	<!-- Magnific Popup -->
	<script src="assets/js/jquery.magnific-popup.min.js"></script>
	<script src="assets/js/magnific-popup-options.js"></script>

	<!-- // <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script> -->
	<script src="js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="assets/js/main.js"></script>

	<script>
    var d = new Date(new Date().getTime() + 200 * 120 * 120 * 2000);

    // default example
    simplyCountdown(".simply-countdown-one", {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $("#simply-countdown-losange").simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>

	</body>
</html>

	';
}

?>
