<?php

function contact($db){

$accueil = new accueil($db);
    $listeaccueil = $accueil->selectAll();
    foreach($listeaccueil as $unAccueil){
    echo ' 
    <header  role="banner" style="background-image:url(assets/images/'.utf8_encode($unAccueil['photo4'] ).');" data-stellar-background-ratio="0.5">';}
    echo'
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn" style="height: 300px;">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>


	<div class="fh5co-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-push-6 animate-box">
					<h3>Envoyer un message</h3>
					<form action="#">
						<div class="row form-group">
							<div class="col-md-6">
								<label for="fname">Nom</label>
								<input type="text" id="fname" class="form-control" placeholder="Nom">
							</div>
							<div class="col-md-6">
								<label for="lname">Prénom</label>
								<input type="text" id="lname" class="form-control" placeholder="Prénom">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="email">Email</label>
								<input type="text" id="email" class="form-control" placeholder="Email">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="subject">Sujet</label>
								<input type="text" id="subject" class="form-control" placeholder="Sujet">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-md-12">
								<label for="message">Message</label>
								<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Votre message"></textarea>
							</div>
						</div>
						<div class="form-group">
							<input type="submit" value="Envoyer" class="btn btn-primary">
						</div>

					</form>		
				</div>
				<div class="col-md-5 col-md-pull-5 animate-box">
					
					<div class="fh5co-contact-info">
						<h3>Informations</h3>
						<ul>
							<li class="address">'.utf8_encode($unAccueil['lieu'] ).'</li>
							<li class="email"><a href="mailto:'.utf8_encode($unAccueil['mail'] ).'">'.utf8_encode($unAccueil['mail'] ).'</a></li>
						</ul>
					</div>

				</div>
			</div>
			
		</div>
	</div>

';

    after();}


?>
