<?php


function menuadmin(){
    echo '   
    <header  role="banner" style="background-image:url(assets/images/img_bg_5.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn" style=" padding-top:110px;">
                            <h2>ADMINISTRATEUR</h2>
                            <div class="simply-countdown simply-countdown-one"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid">
          ';
    
    echo' 
    <nav class="navbar navbar-default sidebar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>      
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                
                    <li ><a href="index.php?page=accueiladmin">GESTION DU SITE :<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon "></span></a></li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">CATÉGORIE <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon "></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listetypeadmin">Liste</a></li>
                            <li><a href="index.php?page=inscriptiontypeadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimetypeadmin">Suppression</a></li>
                            <li><a href="index.php?page=modiftypeadmin">Modification</a></li>

                        </ul>
                    </li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">CADEAUX <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon"></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listecadeauadmin">Liste</a></li>
                            <li><a href="index.php?page=formcadeauadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimecadeauadmin">Suppression</a></li>
                        </ul>
                    </li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">INVITÉS<span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon"></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listeinviteadmin">Liste</a></li>
                            <li><a href="index.php?page=forminviteadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimeinviteadmin">Suppression</a></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">ACCUEIL<span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon"></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=modifaccueiladmin">Modification</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>
    ';
}

function piedadmin(){
   
    echo'</div>';
    after();
}

function accueiladmin(){

menuadmin(); 
echo' 
        <p>Bienvenue dans votre espace administrateur, <br>
Depuis cette interface pour pourrait facilement visualiser, modifier, supprimer et ajouter les différents élèment que comporte ce site. 

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

Lorem ipsum dolor sit amunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	';

piedadmin();
}


function modifaccueiladmin($db){
    menuadmin(); 
    $accueil = new accueil($db);
    $listeaccueil = $accueil->selectAll();

    echo '<table class="table table-striped">
            <THEAD>
                <tr id="titre">
                    <th >nom</th>
                    <th>Sélection</th>
                </tr>
            </THEAD>
            <TBODY>
    ';

    foreach($listeaccueil as $unaccueil){
    
    echo '<tr>
            <td><a href="index.php?page=modificationaccueiladmin&nom='.$unaccueil['nom'].'" >'.utf8_encode($unaccueil['nom']).'</a></td>
            <td><input type="checkbox" name="cocher[]" value="'.$unaccueil['nom'].'"/></td>
          </tr>';
        }
        
    echo '</TBODY>';
    echo '</table>';
    piedadmin();
}


function modificationaccueiladmin($db){
    menuadmin();

    if(isset($_POST['btModifier'])){
        $nom = $_POST['nom'];
        $pere = $_POST['pere'];
        $mere = $_POST['mere'];
        $enfant = $_POST['enfant'];
        $mail = $_POST['mail'];
        $date = $_POST['date'];
        $lieu = $_POST['lieu'];
        $titreprincipal = $_POST['titreprincipal'];
        $soustitre = $_POST['soustitre'];
        $titre1 = $_POST['titre1'];
        $titre2 = $_POST['titre2'];
        $photo1 = $_POST['photo1'];
        $photo2 = $_POST['photo2'];
        $photo3 = $_POST['photo3'];
        $photo4 = $_POST['photo4'];
        $accueil = new accueil($db);
        $nb = $accueil->updateAll($nom, $pere, $mere, $enfant, $mail, $date, $lieu, $titreprincipal, $soustitre, $titre1, $titre2, $photo1, $photo2, $photo3, $photo4);
    }


    if (isset($_GET['nom'])){
        $nom = $_GET['nom'];
    }

    $accueil = new accueil($db);
    $unaccueil = $accueil->selectOne($nom);

    echo'<form action="index.php?page=modificationaccueiladmin" method="post">
        <input type="hidden" name="nom"  value="'.$nom.'" />
        <label for="nom">Nom du père :</label><input type="text" name="pere" value="'.$unaccueil['pere'].'" />
        <br>
        <label for="nom">Nom de la mère :</label><input type="text" name="mere" value="'.$unaccueil['mere'].'" />
 <br>
        <label for="nom">Nom enfant :</label><input type="text" name="enfant" value="'.$unaccueil['enfant'].'" />
 <br>
        <label for="nom">Mail :</label><input type="text" name="mail" value="'.$unaccueil['mail'].'" />
 <br>
        <label for="nom">Date :</label><input type="text" name="date" value="'.$unaccueil['date'].'" />
 <br>
        <label for="nom">Lieu :</label><input type="text" name="lieu" value="'.$unaccueil['lieu'].'" />
 <br>
        <label for="nom">Titre principal :</label><input type="text" name="titreprincipal" value="'.$unaccueil['titreprincipal'].'" />
 <br>
        <label for="nom">sous titre :</label><input type="text" name="soustitre" value="'.$unaccueil['soustitre'].'" />
 <br>
        <label for="nom">Titre 1 :</label><input type="text" name="titre1" value="'.$unaccueil['titre1'].'" />
         <br>
        <label for="nom">Titre 2 :</label><input type="text" name="titre2" value="'.$unaccueil['titre2'].'" />
         <br>

         <label for="nom">Photo 1 :</label>
        <select id="photo1" name="photo1">
            <optgroup label=" - - - SÉLECTIONNER UNE PHOTO - - - ">
                <option value="photo1.jpg">Photo 1</option>
                <option value="photo2.jpg">Photo 2</option>
                <option value="photo3.jpg">Photo 3</option>
                <option value="photo4.jpg">Photo 4</option>
            </optgroup>
        </select>
        <br>
         <label for="nom">Photo 2 :</label>
        <select id="photo2" name="photo2">
            <optgroup label=" - - - SÉLECTIONNER UNE PHOTO - - - ">
                <option value="photo1.jpg">Photo 1</option>
                <option value="photo2.jpg">Photo 2</option>
                <option value="photo3.jpg">Photo 3</option>
                <option value="photo4.jpg">Photo 4</option>
            </optgroup>
        </select>
        <br>
         <label for="nom">Photo 3 :</label>
        <select id="photo3" name="photo3">
            <optgroup label=" - - - SÉLECTIONNER UNE PHOTO - - - ">
                <option value="photo1.jpg">Photo 1</option>
                <option value="photo2.jpg">Photo 2</option>
                <option value="photo3.jpg">Photo 3</option>
                <option value="photo4.jpg">Photo 4</option>
            </optgroup>
        </select>
        <br>
         <label for="nom">Photo 4 :</label>
        <select id="photo4" name="photo4">
            <optgroup label=" - - - SÉLECTIONNER UNE PHOTO - - - ">
                <option value="photo1.jpg">Photo 1</option>
                <option value="photo2.jpg">Photo 2</option>
                <option value="photo3.jpg">Photo 3</option>
                <option value="photo4.jpg">Photo 4</option>
            </optgroup>
        </select>


        <input type="submit" name="btModifier" id="btModifier" value="Modifier">
        </form>';

    piedadmin();
}

/******************************************************************************/
/********************* GESTION INVITES - ADMINISTRATEUR ***********************/
/******************************************************************************/

function forminviteadmin($db){
    menuadmin(); 
    ajouterClient($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}

function ajoutinviteadmin($db){
    menuadmin(); 
    ajoutClient($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


function listeinviteadmin($db){
    menuadmin(); 
    listeinvite($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


function supprimeinviteadmin($db){
    menuadmin(); 
    supprimeInvite($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


/******************************************************************************/
/********************** GESTION CADEAUX- ADMINISTRATEUR ***********************/
/******************************************************************************/

function listecadeauadmin($db){
    menuadmin(); 
    listecadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}

function formcadeauadmin($db){
    menuadmin(); 
    formCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
    piedadmin();     
}

function ajoutcadeauadmin($db){
    
    menuadmin(); 
    ajoutCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
     piedadmin();     
}

function supprimecadeauadmin($db){
    
    menuadmin(); 
    supprimeCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
    piedadmin();        
}






/******************************************************************************/
/******************** GESTION CATÉGORIE- ADMINISTRATEUR ***********************/
/******************************************************************************/



function listetypeadmin($db){
    menuadmin(); 
    listeTypes($db);
    piedadmin();
}

function ajouttypeadmin($db){
    menuadmin(); 
    ajoutType($db);
    piedadmin();
}

function inscriptiontypeadmin($db){
    menuadmin(); 
    inscriptionType($db);
    piedadmin();
}

function supprimetypeadmin($db){
    menuadmin(); 
    supprimeType($db);
    piedadmin();
}

function modiftypeadmin($db){
    menuadmin(); 
    modifType($db);
    piedadmin();
}

function modificationtypeadmin($db){
    menuadmin(); 
    modificationType($db);
    piedadmin();
}


?>
